/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.   See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package l1j.server.server.command.executor;

import java.util.logging.Logger;

import l1j.server.server.GMCommandsConfig;
import l1j.server.server.model.L1Location;
import l1j.server.server.model.L1Teleport;
import l1j.server.server.model.Instance.L1PcInstance;
import l1j.server.server.serverpackets.S_SystemMessage;

public class L1GMRoom implements L1CommandExecutor {
	@SuppressWarnings("unused")
	private static Logger _log = Logger.getLogger(L1GMRoom.class.getName());

	private L1GMRoom() {
	}

	public static L1CommandExecutor getInstance() {
		return new L1GMRoom();
	}

	@Override
	public void execute(L1PcInstance pc, String cmdName, String arg) {
		try {
			int i = 0;
			try {
				i = Integer.parseInt(arg);
			} catch (NumberFormatException e) {
			}

			if (i == 1) {
				new L1Teleport().teleport(pc, 32737, 32796, (short) 99, 5, false); // ���ڹ�
			} else if (i == 2) {
				new L1Teleport().teleport(pc, 32736, 32796, (short) 16896, 4, false); // ����
			} else if (i == 3) {
				new L1Teleport().teleport(pc , 32638, 32955, (short) 0, 5, false); // �ǵ���
			} else if (i == 4) {
				new L1Teleport().teleport(pc , 33440, 32805, (short) 4, 5, false); // ���
			} else if (i == 5) {
				new L1Teleport().teleport(pc , 32894, 32536, (short) 300, 5, false); // �Ƶ�����
			} else if (i == 6) {
				new L1Teleport().teleport(pc , 32614, 32788, (short) 4, 5, false); // �۷��
			} else if (i == 7) {
				new L1Teleport().teleport(pc , 34055, 32281, (short) 4, 5, false); // ����
			} else if (i == 8) {
				new L1Teleport().teleport(pc , 33515, 32858, (short) 4, 5, false); // ������
			} else if (i == 9) {
				new L1Teleport().teleport(pc , 32763, 32817, (short) 622, 5, false); // ���и���
			} else if (i == 10) {
				new L1Teleport().teleport(pc , 32572, 32944, (short) 0, 5, false); // ����
			} else if (i == 11) {
				new L1Teleport().teleport(pc , 33723, 32495, (short) 4, 5, false); // ����
			} else if (i == 12) {
				new L1Teleport().teleport(pc , 32760, 32870, (short) 610, 5, false); // ���ɸ���
			} else if (i == 13) {
				new L1Teleport().teleport(pc , 32805, 32814, (short) 5490, 5, false); // ����
			} else if (i == 14) {
				new L1Teleport().teleport(pc , 32736, 32787, (short) 15, 5, false); // �˼�
			} else if (i == 15) {
				new L1Teleport().teleport(pc , 32735, 32788, (short) 29, 5, false); // ����
			} else if (i == 16) {
				new L1Teleport().teleport(pc , 32730, 32802, (short) 52, 5, false); // ���
			} else if (i == 17) {
				new L1Teleport().teleport(pc , 32572, 32826, (short) 64, 5, false); // ���̳׼�
			} else if (i == 18) {
				new L1Teleport().teleport(pc , 32895, 32533, (short) 300, 5, false); // �Ƶ���
			} else if (i == 19) {
				new L1Teleport().teleport(pc , 33168, 32779, (short) 4, 5, false); // �˼� ��ȣž
			} else if (i == 20) {
				new L1Teleport().teleport(pc , 32623, 33379, (short) 4, 5, false); // ���� ��ȣž
			} else if (i == 21) {
				new L1Teleport().teleport(pc , 33630, 32677, (short) 4, 5, false); // ��� ��ȣž
			} else if (i == 22) {
				new L1Teleport().teleport(pc , 33524, 33394, (short) 4, 5, false); // ���̳� ��ȣž
			} else if (i == 23) {
				new L1Teleport().teleport(pc , 34090, 33260, (short) 4, 5, false); // �Ƶ� ��ȣž
			} else if (i == 24) {
				new L1Teleport().teleport(pc , 32424, 33068, (short) 440, 5, false); // ������
			} else if (i == 25) {
				new L1Teleport().teleport(pc , 32800, 32868, (short) 1001, 5, false); // �����
			} else if (i == 26) {
				new L1Teleport().teleport(pc , 32800, 32856, (short) 1000, 5, false); // �Ǻ�����
			} else if (i == 27) {
				new L1Teleport().teleport(pc , 32630, 32903, (short) 780, 5, false); // �׺��縷
			} else if (i == 28) {
				new L1Teleport().teleport(pc , 32743, 32799, (short) 781, 5, false); // �׺� �Ƕ�̵� ����
			} else if (i == 29) {
				new L1Teleport().teleport(pc , 32735, 32830, (short) 782, 5, false); // �׺� �����ý� ����
			} else if (i == 30) {
				new L1Teleport().teleport(pc , 32734, 32270, (short) 4, 5, false); // �Ǵ�
			} else if (i == 31) {
				new L1Teleport().teleport(pc , 32699, 32819, (short) 82, 5, false); // ����
			} else if (i == 32) {
				new L1Teleport().teleport(pc , 32769, 32770, (short) 56, 5, false); // �Ⱘ4��
			} else if (i == 33) {
				new L1Teleport().teleport(pc , 32929, 32995, (short) 410, 5, false); // ��������				
			} else if (i == 34) {
				new L1Teleport().teleport(pc , 32791, 32691, (short) 1005, 5, false); // ���̵� ��Ÿ��
			} else if (i == 35) {
				new L1Teleport().teleport(pc , 32960, 32840, (short) 1011, 5, false); // ���̵� ��Ǫ����
			} else if (i == 36) {
				new L1Teleport().teleport(pc , 32849, 32876, (short) 1017, 5, false); // ���巹�̵�
			} else if (i == 37) {
				new L1Teleport().teleport(pc , 32725, 32800, (short) 67, 5, false); // �߶��
			} else if (i == 38) {
				new L1Teleport().teleport(pc , 32771, 32831, (short) 65, 5, false); // ��Ǫ��
			} else if (i == 39) {
				new L1Teleport().teleport(pc , 32696, 32824, (short) 37, 5, false); // ���� (���7��)
			} else if (i == 40) {
				new L1Teleport().teleport(pc , 32922, 32812, (short) 430, 5, false); // ���ɹ���
			} else if (i == 41) {
				new L1Teleport().teleport(pc , 32737, 32834, (short) 2004, 5, false); // ����
			} else if (i == 42) {
				new L1Teleport().teleport(pc , 32707, 32846, (short) 2, 5, false); // ����2��
			} else if (i == 43) {
				new L1Teleport().teleport(pc , 32772, 32861, (short) 400, 5, false); // ���빫��
			} else if (i == 44) {
				new L1Teleport().teleport(pc , 32982, 32808, (short) 244, 5, false); // ����
			} else if (i == 45) {
				new L1Teleport().teleport(pc , 32811, 32819, (short) 460, 5, false); // ���2��
			} else if (i == 46) {
				new L1Teleport().teleport(pc , 32724, 32792, (short) 536, 5, false); // ���3��
			} else if (i == 47) {
				new L1Teleport().teleport(pc , 32847, 32793, (short) 532, 5, false); // ���4��
			} else if (i == 48) {
				new L1Teleport().teleport(pc , 32843, 32693, (short) 550, 5, false); // ���ڹ���
			} else if (i == 49) {
				new L1Teleport().teleport(pc , 32781, 32801, (short) 558, 5, false); // ����
			} else if (i == 50) {
				new L1Teleport().teleport(pc , 32731, 32862, (short) 784, 5, false); // ����
			} else if (i == 51) {
				new L1Teleport().teleport(pc , 32728, 32704, (short) 4, 5, false); // �տ� 1
			} else if (i == 52) {
				new L1Teleport().teleport(pc , 32827, 32658, (short) 4, 5, false); // �տ� 2
			} else if (i == 53) {
				new L1Teleport().teleport(pc , 32852, 32713, (short) 4, 5, false); // �տ� 3
			} else if (i == 54) {
				new L1Teleport().teleport(pc , 32914, 33427, (short) 4, 5, false); // �տ� 4
			} else if (i == 55) {
				new L1Teleport().teleport(pc , 32962, 33251, (short) 4, 5, false); // �տ� 5
			} else if (i == 56) {
				new L1Teleport().teleport(pc , 32908, 33169, (short) 4, 5, false); // �տ� 6
			} else if (i == 57) {
				new L1Teleport().teleport(pc , 34272, 33361, (short) 4, 5, false); // �տ� 7
			} else if (i == 58) {
				new L1Teleport().teleport(pc , 34258, 33202, (short) 4, 5, false); // �տ� 8
			} else if (i == 59) {
				new L1Teleport().teleport(pc , 34225, 33313, (short) 4, 5, false); // �տ� 9
			} else if (i == 60) {
				new L1Teleport().teleport(pc , 32682, 32892, (short) 5167, 5, false); // �Ǹ�����
			} else if (i == 61) {
				new L1Teleport().teleport(pc , 32862, 32862, (short) 537, 5, false); // �⸣Ÿ��
			} else if (i == 62) {
				new L1Teleport().teleport(pc , 32738, 32448, (short) 4, 5, false); // ȭ����
			} else if (i == 63) {
				new L1Teleport().teleport(pc , 32797, 32285, (short) 4, 5, false); // ������ž
			} else if (i == 64) {
				new L1Teleport().teleport(pc , 33052, 32339, (short) 4, 5, false); // ������
			} else if (i == 65) {
				new L1Teleport().teleport(pc, 32738, 32872, (short) 2236, 5, false); // �����������Ʈ
			} else {
				L1Location loc = GMCommandsConfig.ROOMS.get(arg.toLowerCase());
				if (loc == null) {
					pc.sendPackets(new S_SystemMessage("==================<��ȯ ���>==================="));
					pc.sendPackets(new S_SystemMessage("\\aD1.GM1 2.GM2 3.�ǵ��� 4.��� 5.�Ƶ����� 6.�۷�� 7.����"));
					pc.sendPackets(new S_SystemMessage("\\aD8.������ 9.���и� 10.���� 11.���� 12.���� 13.����"));
					pc.sendPackets(new S_SystemMessage("\\aL14.��Ʈ�� 15.���ټ� 16.����� 17.���̼� 18.�Ƶ���"));
					pc.sendPackets(new S_SystemMessage("\\aL19.��ȣž 20.��ȣž 21.��ȣž 22.��ȣž 23.��ȣž"));
					pc.sendPackets(new S_SystemMessage("\\aH24.�ؼ� 25.������ 26.�Ǻ����� 27.�׺� 28.�Ƕ�̵�"));
					pc.sendPackets(new S_SystemMessage("\\aH29.�Ƕ�̵� 30.�Ǵн� 31.���� 32.�Ⱘ4�� 33.������"));
					pc.sendPackets(new S_SystemMessage("\\aD34.��Ÿ 35.��Ǫ 36.���� 37.�߶� 38.����Ǫ 39.����"));
					pc.sendPackets(new S_SystemMessage("\\aL40.���� 41.���� 42.����2�� 43.���빫�� 44.����"));
					pc.sendPackets(new S_SystemMessage("\\aL45.���2�� 46.���3�� 47.���4�� 48.���� 49.����"));
					pc.sendPackets(new S_SystemMessage("\\aL50.���극�� 51~59.�տ� 60.�Ǹ����� 61.�⸣Ÿ��"));
					pc.sendPackets(new S_SystemMessage("\\aL62.ȭ���θ��� 63.��ũ�� 64.������ 65.�����������Ʈ"));
					return;
				}
				new L1Teleport().teleport(pc, loc.getX(), loc.getY(), (short) loc
						.getMapId(), 5, false);
			}
			if (i > 0 && i < 33) {
				pc.sendPackets(new S_SystemMessage("��� ��ȯ(" + i + ")������ �̵��߽��ϴ�."));
			}
		} catch (Exception exception) {
			pc.sendPackets(new S_SystemMessage(".��ȯ [��Ҹ�]�� �Է� ���ּ���.(��Ҹ��� GMCommands.xml�� ����)"));
		}
	}
}
