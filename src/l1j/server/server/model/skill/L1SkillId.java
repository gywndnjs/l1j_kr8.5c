package l1j.server.server.model.skill;

public class L1SkillId {
	
	public static final int RANK_BUFF_1 = 40001;
	public static final int RANK_BUFF_2 = 40002;
	public static final int RANK_BUFF_3 = 40003;
	public static final int RANK_BUFF_4 = 40004;
	public static final int RANK_BUFF_5 = 40005;
	public static final int 나루토감사캔디 = 80005;
	public static final int ABYSS_LIGHTNING_TIME = 80006;
	public static final int 레벨업보너스 = 80007;
	public static final int DRAGON_PUPLE = 80008;
	public static final int DRAGON_TOPAZ = 80009;
	public static final int 신고딜레이 = 80010;
	
	public static final int 천하장사버프 = 1541;
	public static final int STATUS_안전모드 = 1032;
	public static final int SKILLS_BEGIN = 1;

	public static final int CLANBUFF_YES = 7789; // 혈맹버프

	public static final int God_buff = 4914; // 흑사버프

	public static final int STATUS_PET_FOOD = 1019;

	public static final int NO_DIS = 60001; // 디스중첩

	public static final int DELAY = 9999; // 광역스턴딜레이

	public static final int BRAVE_AVATAR_1ST = 7100;
	public static final int BRAVE_AVATAR_2ND = 7101;
	public static final int BRAVE_AVATAR_3RD = 7102;
	public static final int 주군의버프 = 10534;

	public static final int 피씨방_버프 = 777888;

	public static final int 강화버프_활력 = 56000;
	public static final int 강화버프_공격 = 56001;
	public static final int 강화버프_방어 = 56002;
	public static final int 강화버프_마법 = 56003;
	public static final int 강화버프_스턴 = 56004;
	public static final int 강화버프_홀드 = 56005;
	public static final int 강화버프_힘 = 56006;
	public static final int 강화버프_덱스 = 56007;
	public static final int 강화버프_인트 = 56008;
	
	
	public static final int CLAN_BUFF1 = 505; //혈맹버프
	public static final int CLAN_BUFF2 = 506;
	public static final int CLAN_BUFF3 = 507;
	public static final int CLAN_BUFF4 = 508;
	
	// 탐
	public static final int Tam_Fruit1 = 7791;
	public static final int Tam_Fruit2 = 7792;
	public static final int Tam_Fruit3 = 7793;
	public static final int Tam_Fruit4 = 7794;
	public static final int Tam_Fruit5 = 7795;
	/*
	 * Regular Magic Lv1-10
	 */
	// 1단계 일반마법
	public static final int HEAL = 1; // E: LESSER_HEAL
	public static final int LIGHT = 2;
	public static final int SHIELD = 3;
	public static final int ENERGY_BOLT = 4;
	public static final int TELEPORT = 5;
	public static final int ICE_DAGGER = 6;
	public static final int WIND_CUTTER = 7; // E: WIND_SHURIKEN
	public static final int HOLY_WEAPON = 8;

	// 2단계 일반마법
	public static final int CURE_POISON = 9;
	public static final int CHILL_TOUCH = 10;
	public static final int CURSE_POISON = 11;
	public static final int ENCHANT_WEAPON = 12;
	public static final int DETECTION = 13;
	public static final int DECREASE_WEIGHT = 14;
	public static final int FIRE_ARROW = 15;
	public static final int STALAC = 16;

	// 3단계 일반마법
	public static final int LIGHTNING = 17;
	public static final int TURN_UNDEAD = 18;
	public static final int EXTRA_HEAL = 19; // E: HEAL
	public static final int CURSE_BLIND = 20;
	public static final int BLESSED_ARMOR = 21;
	public static final int FROZEN_CLOUD = 22;
	public static final int WEAK_ELEMENTAL = 23; // E: REVEAL_WEAKNESS

	// 4단계 일반마법 // none = 24
	public static final int FIREBALL = 25;
	public static final int PHYSICAL_ENCHANT_DEX = 26; // E: ENCHANT_DEXTERITY
	public static final int WEAPON_BREAK = 27;
	public static final int VAMPIRIC_TOUCH = 28;
	public static final int SLOW = 29;
	public static final int EARTH_JAIL = 30;
	public static final int COUNTER_MAGIC = 31;
	public static final int MEDITATION = 32;

	// 5단계 일반마법
	public static final int CURSE_PARALYZE = 33;
	public static final int CALL_LIGHTNING = 34;
	public static final int GREATER_HEAL = 35;
	public static final int TAMING_MONSTER = 36; // E: TAME_MONSTER
	public static final int REMOVE_CURSE = 37;
	public static final int CONE_OF_COLD = 38;
	public static final int MANA_DRAIN = 39;
	public static final int DARKNESS = 40;

	// 6단계 일반마법
	public static final int CREATE_ZOMBIE = 41;
	public static final int PHYSICAL_ENCHANT_STR = 42; // E: ENCHANT_MIGHTY
	public static final int HASTE = 43;
	public static final int CANCELLATION = 44; // E: CANCEL MAGIC
	public static final int ERUPTION = 45;
	public static final int SUNBURST = 46;
	public static final int WEAKNESS = 47;
	public static final int BLESS_WEAPON = 48;

	// 7단계 일반마법
	public static final int HEAL_ALL = 49; // E: HEAL_PLEDGE
	public static final int ICE_LANCE = 50;
	public static final int SUMMON_MONSTER = 51;
	public static final int HOLY_WALK = 52;
	public static final int TORNADO = 53;
	public static final int GREATER_HASTE = 54;
	public static final int BERSERKERS = 55;
	public static final int DISEASE = 56;

	// 8단계 일반마법
	public static final int FULL_HEAL = 57;
	public static final int FIRE_WALL = 58;
	public static final int BLIZZARD = 59;
	public static final int INVISIBILITY = 60;
	public static final int RESURRECTION = 61;
	public static final int EARTHQUAKE = 62;
	public static final int LIFE_STREAM = 63;
	public static final int SILENCE = 64;

	// 9단계 일반마법
	public static final int LIGHTNING_STORM = 65;
	public static final int FOG_OF_SLEEPING = 66;
	public static final int SHAPE_CHANGE = 67; // E: POLYMORPH
	public static final int IMMUNE_TO_HARM = 68;
	public static final int MASS_TELEPORT = 69;
	public static final int FIRE_STORM = 70;
	public static final int DECAY_POTION = 71;
	public static final int COUNTER_DETECTION = 72;

	// 10단계 일반마법
	public static final int DEATH_HEAL = 73;
	public static final int METEOR_STRIKE = 74;
	public static final int GREATER_RESURRECTION = 75;
	public static final int GREATER_SLOW = 76;
	public static final int DISINTEGRATE = 77; // E: DESTROY
	public static final int ABSOLUTE_BARRIER = 78;
	public static final int ADVANCE_SPIRIT = 79;
	public static final int FREEZING_BLIZZARD = 80;

	// none = 81 - 86
	/*
	 * Knight skills
	 */
	public static final int SHOCK_STUN = 87; // E: STUN_SHOCK
	public static final int REDUCTION_ARMOR = 88;
	public static final int BOUNCE_ATTACK = 89;
	public static final int SOLID_CARRIAGE = 90;
	public static final int COUNTER_BARRIER = 91;
	public static final int ABSOLUTE_BLADE = 92;

	// none = 92-96
	/*
	 * Dark Spirit Magic
	 */
	public static final int BLIND_HIDING = 97;
	public static final int ENCHANT_VENOM = 98;
	public static final int SHADOW_ARMOR = 99;
	public static final int BRING_STONE = 100;
	public static final int MOVING_ACCELERATION = 101; // E: PURIFY_STONE
	public static final int BURNING_SPIRIT = 102;
	public static final int DARK_BLIND = 103;
	public static final int VENOM_RESIST = 104;
	public static final int DOUBLE_BRAKE = 105;
	public static final int UNCANNY_DODGE = 106;
	public static final int SHADOW_FANG = 107;
	public static final int FINAL_BURN = 108;
	public static final int DRESS_MIGHTY = 109;
	public static final int DRESS_DEXTERITY = 110;
	public static final int DRESS_EVASION = 111;
	/** 아머 브레이크 */
	public static final int ARMOR_BRAKE = 112;
	public static final int weapon_F = 50010;// 군검이펙
	public static final int ASSASSIN = 233;
	public static final int BLAZING_SPIRITS  = 241;
	// none = 112
	/*
	 * Royal Magic
	 */
	public static final int TRUE_TARGET = 113;
	public static final int GLOWING_AURA = 114;
	public static final int SHINING_AURA = 115;
	public static final int CALL_CLAN = 116; // E: CALL_PLEDGE_MEMBER
	public static final int BRAVE_AURA = 117;
	public static final int RUN_CLAN = 118;

	public static final int BRAVE_AVATAR = 120;
	public static final int GRACE_AVATAR = 122;

	// unknown = 119 - 120
	// none = 121 - 128
	/*
	 * Spirit Magic
	 */
	public static final int RESIST_MAGIC = 129;
	public static final int BODY_TO_MIND = 130;
	public static final int TELEPORT_TO_MATHER = 131;
	public static final int TRIPLE_ARROW = 132;
	public static final int ELEMENTAL_FALL_DOWN = 133;
	public static final int COUNTER_MIRROR = 134;
	public static final int SOUL_BARRIER = 135;

	// none = 135 - 136
	public static final int CLEAR_MIND = 137;
	public static final int RESIST_ELEMENTAL = 138;

	// none = 139 - 144
	public static final int RETURN_TO_NATURE = 145;
	public static final int BLOODY_SOUL = 146; // E: BLOOD_TO_SOUL
	public static final int ELEMENTAL_PROTECTION = 147; // E:PROTECTION_FROM_ELEMENTAL
	public static final int FIRE_WEAPON = 148;
	public static final int WIND_SHOT = 149;
	public static final int WIND_WALK = 150;
	public static final int EARTH_SKIN = 151;
	public static final int ENTANGLE = 152;
	public static final int ERASE_MAGIC = 153;
	public static final int LESSER_ELEMENTAL = 154; // E:SUMMON_LESSER_ELEMENTAL
	public static final int DANCING_BLADES = 155; // E: BLESS_OF_FIRE
	public static final int STORM_EYE = 156; // E: EYE_OF_STORM
	public static final int EARTH_BIND = 157;
	public static final int NATURES_TOUCH = 158;
	public static final int EARTH_GUARDIAN = 159; // E: BLESS_OF_EARTH
	public static final int AQUA_PROTECTER = 160;
	public static final int AREA_OF_SILENCE = 161;
	public static final int GREATER_ELEMENTAL = 162; // E:SUMMON_GREATER_ELEMENTAL
	public static final int BURNING_WEAPON = 163;
	public static final int NATURES_BLESSING = 164;
	public static final int CALL_OF_NATURE = 165; // E: NATURES_MIRACLE
	public static final int STORM_SHOT = 166;
	public static final int WIND_SHACKLE = 167;
	public static final int IRON_SKIN = 168;
	public static final int EXOTIC_VITALIZE = 169;
	public static final int WATER_LIFE = 170;
	public static final int ELEMENTAL_FIRE = 171;
	public static final int STORM_WALK = 172;
	public static final int POLLUTE_WATER = 173;
	public static final int STRIKER_GALE = 174;
	public static final int SOUL_OF_FLAME = 175;
	public static final int ADDITIONAL_FIRE = 176;

	/*
	 * 용기사 스킬
	 */

	public static final int DRAGON_SKIN = 181; // 드래곤 스킨

	public static final int BURNING_SLASH = 182;

	public static final int GUARD_BREAK = 183; // 가드 브레이크

	public static final int MAGMA_BREATH = 184;

	public static final int SCALES_EARTH_DRAGON = 185;

	public static final int BLOOD_LUST = 186; // 블러드 러스트

	public static final int FOU_SLAYER = 187;

	public static final int FEAR = 188; // 피어

	public static final int MAGMA_ARROW = 189;

	public static final int SCALES_WATER_DRAGON = 190;

	public static final int MORTAL_BODY = 191;

	public static final int THUNDER_GRAB = 192;

	public static final int HORROR_OF_DEATH = 193;

	public static final int EYE_OF_DRAGON = 194;

	public static final int SCALES_FIRE_DRAGON = 195;
	
	public static final int DESTROY = 196;

	/*
	 * 환술사 스킬
	 */
	/** 1단계 */
	public static final int MIRROR_IMAGE = 201;
	public static final int CONFUSION = 202;
	public static final int SMASH = 203;
	public static final int IllUSION_OGRE = 204;
	public static final int CUBE_IGNITION = 205;

	/** 2단계 */
	public static final int CONCENTRATION = 206;
	public static final int MIND_BREAK = 207;
	public static final int BONE_BREAK = 208;
	public static final int IllUSION_LICH = 209;
	public static final int CUBE_QUAKE = 210;

	/** 3단계 */
	public static final int PATIENCE = 211;
	public static final int PHANTASM = 212;
	public static final int IZE_BREAK = 213;
	public static final int IllUSION_DIAMONDGOLEM = 214;
	public static final int CUBE_SHOCK = 215;

	/** 4단계 */
	public static final int INSIGHT = 216;
	public static final int PANIC = 217;
	public static final int REDUCE_WEIGHT = 218;
	public static final int IllUSION_AVATAR = 219;
	public static final int CUBE_BALANCE = 220;
	
	public static final int IMPACT = 222;

	/** 전사스킬 */
	public static final int HOWL = 225;
	public static final int GIGANTIC = 226;
	public static final int POWERRIP = 228;
	public static final int TOMAHAWK = 229;
	public static final int DESPERADO = 230;
	public static final int TITANL_RISING = 231;
	/** 패시브 **/
	public static final int CRASH = 233;
	public static final int FURY = 234;
	public static final int SLAYER = 235;
	public static final int ARMORGUARD = 237;
	public static final int TITANL_ROOK = 238;
	public static final int TITANL_BLICK = 239;
	public static final int TITANL_MAGIC = 240;
	/** 패시브 **/

	/** 전사스킬 */

	public static final int SKILLS_END = 240;

	/*
	 * Status
	 */
	public static final int STATUS_BEGIN = 1000;
	public static final int STATUS_BRAVE = 1000;
	public static final int STATUS_HASTE = 1001;
	public static final int STATUS_BLUE_POTION = 1002;
	public static final int STATUS_UNDERWATER_BREATH = 1003;
	public static final int STATUS_WISDOM_POTION = 1004;
	public static final int STATUS_CHAT_PROHIBITED = 1005;
	public static final int STATUS_POISON = 1006;
	public static final int STATUS_POISON_SILENCE = 1007;
	public static final int STATUS_POISON_PARALYZING = 1008;
	public static final int STATUS_POISON_PARALYZED = 1009;
	public static final int STATUS_CURSE_PARALYZING = 1010;
	public static final int STATUS_CURSE_PARALYZED = 1011;
	public static final int STATUS_FLOATING_EYE = 1012;
	public static final int STATUS_HOLY_WATER = 1013;
	public static final int STATUS_HOLY_MITHRIL_POWDER = 1014;
	public static final int STATUS_HOLY_WATER_OF_EVA = 1015;
	public static final int STATUS_ELFBRAVE = 1016;
	public static final int STATUS_CANCLEEND = 1016;
	public static final int STATUS_CURSE_BARLOG = 1017;
	public static final int STATUS_CURSE_YAHEE = 1018;
	public static final int STATUS_TOMAHAWK = 1020;
	public static final int STATUS_END = 1020;
	public static final int GMSTATUS_BEGIN = 2000;
	public static final int GMSTATUS_INVISIBLE = 2000;
	public static final int GMSTATUS_HPBAR = 2001;
	public static final int GMSTATUS_SHOWTRAPS = 2002;
	public static final int GMSTATUS_END = 2002;
	public static final int COOKING_NOW = 2999;
	public static final int COOKING_BEGIN = 3000;

	/** 1차요리 효과 (노멀) */
	public static final int COOKING_1_0_N = 3000;
	public static final int COOKING_1_1_N = 3001;
	public static final int COOKING_1_2_N = 3002;
	public static final int COOKING_1_3_N = 3003;
	public static final int COOKING_1_4_N = 3004;
	public static final int COOKING_1_5_N = 3005;
	public static final int COOKING_1_6_N = 3006;
	public static final int COOKING_1_7_N = 3007;

	/** 2차요리 효과 (노멀) */
	public static final int COOKING_1_8_N = 3008;
	public static final int COOKING_1_9_N = 3009;
	public static final int COOKING_1_10_N = 3010;
	public static final int COOKING_1_11_N = 3011;
	public static final int COOKING_1_12_N = 3012;
	public static final int COOKING_1_13_N = 3013;
	public static final int COOKING_1_14_N = 3014;
	public static final int COOKING_1_15_N = 3015;

	/** 3차요리 효과 (노멀) */
	public static final int COOKING_1_16_N = 3016;
	public static final int COOKING_1_17_N = 3017;
	public static final int COOKING_1_18_N = 3018;
	public static final int COOKING_1_19_N = 3019;
	public static final int COOKING_1_20_N = 3020;
	public static final int COOKING_1_21_N = 3021;
	public static final int COOKING_1_22_N = 3022;
	public static final int COOKING_1_23_N = 3023;

	/** 1차요리 효과 (환상) */
	public static final int COOKING_1_0_S = 3050;
	public static final int COOKING_1_1_S = 3051;
	public static final int COOKING_1_2_S = 3052;
	public static final int COOKING_1_3_S = 3053;
	public static final int COOKING_1_4_S = 3054;
	public static final int COOKING_1_5_S = 3055;
	public static final int COOKING_1_6_S = 3056;
	public static final int COOKING_1_7_S = 3057;

	/** 2차요리 효과 (환상) */
	public static final int COOKING_1_8_S = 3058;
	public static final int COOKING_1_9_S = 3059;
	public static final int COOKING_1_10_S = 3060;
	public static final int COOKING_1_11_S = 3061;
	public static final int COOKING_1_12_S = 3062;
	public static final int COOKING_1_13_S = 3063;
	public static final int COOKING_1_14_S = 3064;
	public static final int COOKING_1_15_S = 3065;

	/** 3차요리 효과 (환상) */
	public static final int COOKING_1_16_S = 3066;
	public static final int COOKING_1_17_S = 3067;
	public static final int COOKING_1_18_S = 3068;
	public static final int COOKING_1_19_S = 3069;
	public static final int COOKING_1_20_S = 3070;
	public static final int COOKING_1_21_S = 3071;
	public static final int COOKING_1_22_S = 3072;
	public static final int COOKING_1_23_S = 3073;

	public static final int COOK_STR = 3074;
	public static final int COOK_DEX = 3075;
	public static final int COOK_INT = 3076;
	public static final int COOK_GROW = 3077;
	
	public static final int COOKING_END = 3080;
	
	public static final int 나루터감사캔디 = 3078;
	public static final int 메티스요리 = 3079;
	public static final int 메티스스프 = 3080;
	
	public static final int STATUS_FREEZE = 10071;
	public static final int CURSE_PARALYZE2 = 10101;
	public static final int STATUS_IGNITION = 20075;
	public static final int STATUS_QUAKE = 20076;
	public static final int STATUS_SHOCK = 20077;
	public static final int STATUS_BALANCE = 20078;
	public static final int STATUS_FRUIT = 20079;
	public static final int STATUS_OVERLAP = 20080;
	public static final int EXP_POTION = 20081;
	public static final int STATUS_BLUE_POTION2 = 20082;
	public static final int STATUS_DESHOCK = 20083;
	public static final int STATUS_CUBE = 20084;
	public static final int STATUS_CASHSCROLL = 6993;
	public static final int STATUS_CASHSCROLL2 = 6994;
	public static final int STATUS_CASHSCROLL3 = 6995;

	public static final int MOB_SLOW_18 = 30000; // 슬로우 18번모션
	public static final int MOB_SLOW_1 = 30001; // 슬로우 1번모션
	public static final int MOB_CURSEPARALYZ_19 = 30002; // 커스 19번모션
	public static final int MOB_COCA = 30003; // 코카트리스 얼리기공격
	public static final int MOB_BASILL = 30004; // 바실리스크 얼리기에볼
	public static final int MOB_RANGESTUN_19 = 30005; // 범위스턴 19번모션
	public static final int MOB_RANGESTUN_18 = 30006; // 범위스턴 18번모션
	public static final int MOB_CURSEPARALYZ_18 = 30007; // 커스 18번모션
	public static final int MOB_DISEASE_30 = 30008; // 디지즈 30번모션
	public static final int MOB_WEAKNESS_1 = 30009; // 위크니스 1번모션
	public static final int MOB_RANGESTUN_20 = 30010;
	public static final int MOB_DISEASE_1 = 30079; // 디지즈 1번모션
	public static final int MOB_SHOCKSTUN_30 = 30081; // 쇼크스턴 30번모션
	public static final int MOB_WINDSHACKLE_1 = 30084; // 윈드셰클 1번모션
	public static final int Mob_RANGESTUN_30 = 40007; // 자드스케 범위스턴 30번모션 <<추가

	public static final int ANTI_DISINTEGRATE = 50001; // 안티 디스
	public static final int CHAINSWORD1 = 50002;
	public static final int CHAINSWORD2 = 50003;
	public static final int CHAINSWORD3 = 50004;
	public static final int ANTI_FINAL_BURN = 50005; // 안티 디스

	public static final int COMA_A = 50006;
	public static final int COMA_B = 50007;
	public static final int ANTI_METEOR = 50008; // 안티 디스
	public static final int LINDBIOR_SPIRIT_EFFECT = 50009;
	public static final int BUFF_CRAY = 50011; // 크레이버프
	/** 버프 파푸리온 사엘 추가 **/
	public static final int BUFF_SAEL = 10499;
	public static final int BUFF_GUNTER = 120384;

	public static final int FEATHER_BUFF_A = 22000; // 운세버프(매우좋음)
	public static final int FEATHER_BUFF_B = 22001; // 운세버프(좋음)
	public static final int FEATHER_BUFF_C = 22002; // 운세버프(보통)
	public static final int FEATHER_BUFF_D = 22003; // 운세버프(나쁨)
	public static final int SetBuff = 90008; // 셋 버프

	/** 드래곤 리뉴얼 관련 스킬 **/
	public static final int ANTA_MAAN = 7671; // 지룡의 마안 7671
	public static final int FAFU_MAAN = 7672; // 수룡의 마안 7672
	public static final int LIND_MAAN = 7673; // 풍룡의 마안 7673
	public static final int VALA_MAAN = 7674; // 화룡의 마안 7674
	public static final int BIRTH_MAAN = 7675; // 탄생의 마안 7675
	public static final int SHAPE_MAAN = 7676; // 형상의 마안 7676
	public static final int LIFE_MAAN = 7678; // 생명의 마안 7678

	public static final int ANTA_MESSAGE_1 = 22020; // 안타[용언1 / 캔슬 + 마비 -> 오브 모크! 케 네시]
	public static final int ANTA_MESSAGE_2 = 22021; // 안타[용언2 / 블레스+독/ 오브 모크! 켄 로우]
	public static final int ANTA_MESSAGE_3 = 22022; // 안타[용언3 / 왼오펀치+고함/ 오브 모크! 티기르]
	public static final int ANTA_MESSAGE_4 = 22023; // 안타[용언4 / 펀치+블레스/ 오브 모크! 켄 티기르]
	public static final int ANTA_MESSAGE_5 = 22024; // 안타[용언5 / 고함+블레스/ 오브 모크! 루오타]
	public static final int ANTA_MESSAGE_6 = 22025; // 안타[용언6 / 스턴+점프/ 오브 모크! 뮤즈삼]
	public static final int ANTA_MESSAGE_7 = 22026; // 안타[용언7 / 스턴+발작/ 오브 모크! 너츠삼]
	public static final int ANTA_MESSAGE_8 = 22027; // 안타[용언8 / 스턴+발+점/ 오브 모크! 티프삼]
	public static final int ANTA_MESSAGE_9 = 22028; // 안타[용언9 / 웨폰+블레스/ 오브 모크! 리라프]
	public static final int ANTA_MESSAGE_10 = 22029; // 안타[용언10 / 웨폰+마비/ 오브 모크! 세이 라라프]
	public static final int ANTA_CANCELLATION = 22030; // 안타[범위캔슬 / 6칸 / 8명]
	public static final int ANTA_SHOCKSTUN = 22031; // 안타[범위스턴 / 4칸]
	public static final int ANTA_WEAPON_BREAK = 22032; // 안타[범위웨폰 / 4칸]
	public static final int PREDICATEDELAY = 22033; // 용언스킬딜레이

	public static final int PAP_FIVEPEARLBUFF = 22035; // 파푸[오색진주 파괴 버프]
	public static final int PAP_MAGICALPEARLBUFF = 22036; // 파푸[신비한오색진주 파괴버프]
	public static final int PAP_DEATH_PORTION = 22037; // 파푸[데스 포션 버프]
	public static final int PAP_DEATH_HELL = 22038; // 파푸[데스 힐 버프]
	public static final int PAP_REDUCE_HELL = 22039; // 파푸[리듀스 힐 버프]

	public static final int PAP_PREDICATE1 = 22041; // 파푸[용언1:리오타! 피로이 나! [오색 진주 / 신비한 오색 진주 / 토르나 소환]
	public static final int PAP_PREDICATE3 = 22043; // 파푸[용언3:리오타! 라나 오이므! [데스포션]
	public static final int PAP_PREDICATE5 = 22045; // 파푸[용언5:리오타! 네나 우누스! [리듀스 힐 + 머리 공격 + 아이스 브레스]
	public static final int PAP_PREDICATE6 = 22046; // 파푸[용언6:리오타! 테나 웨인라크! [데스 힐 + 꼬리 공격 + 아이스 브레스]
	public static final int PAP_PREDICATE7 = 22047; // 파푸[용언7:리오타! 라나 폰폰! [캔슬레이션 + 오른속 2번 ] [범위 X]
	public static final int PAP_PREDICATE8 = 22048; // 파푸[용언8:리오타! 레포 폰폰! [웨폰브레이크 + 왼손 2번 ] [범위 X]
	public static final int PAP_PREDICATE9 = 22049; // 파푸[용언9:리오타! 테나 론디르 ! [꼬리 2연타 + 아이스 브레스][범위 X]
	public static final int PAP_PREDICATE11 = 22051; // 파푸[용언11:리오타! 오니즈 웨인라크! [매스 캔슬레이션 + 데스 힐 + 아이스 미티어 + 아이스 이럽션] [범위 O]
	public static final int PAP_PREDICATE12 = 22052; // 파푸[용언12:리오타! 오니즈 쿠스온 웨인라크! [매스 캔슬레이션 + 데스힐 + 아이스 미티어 + 발작] [범위 0]

	public static final int ANTA_BUFF = 22015; // 안타라스 혈흔
	public static final int FAFU_BUFF = 22016; // 파푸리온 혈흔
	public static final int STATUS_DRAGON_PEARL = 22017; // 드래곤 진주
	public static final int EMERALD_YES = 22018; // 드래곤 에메랄드
	public static final int EMERALD_NO = 22019; // 드래곤 에메랄드
	public static final int OMAN_STUN = 22055; // 오만보스 광역 스턴
	public static final int OMAN_CANCELLATION = 22056; // 오만보스 광역 캔슬
	public static final int ICE_ERUPTION = 22058; // 극한의 무기 아이스 이럽션
	public static final int RIND_BUFF = 22060;
	public static final int VALA_BUFF = 22061;
	
	public static final int VALA_INTRO_BUFF = 22062;

	public static final int RINDVIOR_WIND_SHACKLE = 7001; // 윈드세클
	public static final int RINDVIOR_PREDICATE_CANCELLATION = 7002; // 리콜 소환
	public static final int RINDVIOR_TORNADO_FORE = 7003; // 회오리 4개 전체 마법
	public static final int RINDVIOR_WEAPON = 7004; // 웨폰
	public static final int RINDVIOR_BOW = 7005;
	// 7006 라이트닝 스톰
	public static final int RINDVIOR_WIND_SHACKLE_1 = 7007;
	public static final int RINDVIOR_WEAPON_2 = 7008;
	public static final int RINDVIOR_STORM = 7009;
	public static final int RINDVIOR_CANCELLATION = 7010;
	// 7011 브레스
	// 7012 라이트닝 스톰 SILENCE
	public static final int RINDVIOR_SILENCE = 7013;
	public static final int RINDVIOR_SUMMON_MONSTER = 7018;
	public static final int RINDVIOR_PREDICATE = 7019;
	public static final int RINDVIOR_SUMMON_MONSTER_CLOUD = 7023;

	// 피닉스
	public static final int PHOENIX_CANCELLATION = 7024;
	public static final int PHOENIX_SUMMON_MONSTER = 7025;

	// 이프리트
	public static final int EFRETE_SUMMON_MONSTER = 7028;

	// 흑장로
	public static final int BLACKELDER_DEATH_POTION = 7030;
	public static final int BLACKELDER_DEATH_HELL = 7031;
	public static final int BLACKELDER = 7037; // [ 라이트닝 / 리치 오로라 / 검은 마법 ]

	// 드레이크
	public static final int DRAKE_WIND_SHACKLE = 7035;
	public static final int DRAKE_MASSTELEPORT = 7036;

	// 사막 보스 [ 광역 마법 ]
	public static final int DESERT_SKILL1 = 7041; // 광역 커스 패럴라이즈
	public static final int DESERT_SKILL2 = 7042; // 광역 어스 바인드
	public static final int DESERT_SKILL3 = 7043; // 광역 마나 드레인
	public static final int DESERT_SKILL4 = 7044; // 광역 독
	public static final int DESERT_SKILL5 = 7045; // 커스/디케이/다크니스/디지즈/위크니스
	public static final int DESERT_SKILL6 = 7046; // 광역 다크니스
	public static final int DESERT_SKILL7 = 7047; // 광역 포그 오브 슬리핑
	public static final int DESERT_SKILL8 = 7048; // 에르자베 토네이도
	public static final int DESERT_SKILL9 = 7049; // 에르자베 서먼 몬스터
	public static final int DESERT_SKILL10 = 7050; // 에르자베 토네이도 스폰
	public static final int Sand_worms     = 7055;//샌드웜 이럽션
	public static final int Sand_worms1     = 7056;//샌드웜 이럽션
	public static final int Sand_worms2     = 7057;//샌드웜 이럽션
	public static final int Sand_worms3     = 7058;//샌드웜 이럽션
	
	//미소피아 버프
	public static final int Matiz_Buff1 = 7600; //경험치보너스 10%
	public static final int Matiz_Buff2 = 7601; //방어 ( mr10+데미지감소+2,HP+100,HP리젠 2)
	public static final int Matiz_Buff3 = 7602; //공격 (근,원거리 데미지+3 ,SP+3 , MP+50 , 엠피회복 +2); 
	public static final int COMBO_BUFF = 80006; // 콤보
}